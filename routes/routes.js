var express = require('express');
var app = express();

var routes = require('./index');
var users = require('./users');

app.use('/', routes);
app.use('/users', users);


module.exports = app;