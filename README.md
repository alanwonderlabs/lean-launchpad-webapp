# Lean Launchpad Web Application

# Technology
  - Express Framework
  
  
# Requirements
  - Node js
  
# Running

Install dependencies :

        $ npm install
        
Run migrations :

        $ node_modules/.bin/sequelize db:migrate
        
To run app :

        $ npm start

# Testing

We have added some Mocha based test. You can run them by npm test. To run all tests :

        $ npm test
        

